# Contact-list-react

## Table of contents

- [Instructions](#Instructions)
- [Description](#Description)
- [Technologies](#Technologies)

## Instructions

To run the project locally:

- First clone this repository

```bash
$ git clone https://gitlab.com/p.kwak/contacts-list-react.git
```

- Then install the dependencies:

Make sure you already have [`nodejs`](https://nodejs.org/en/) & [`npm`](https://www.npmjs.com/) installed in your system.

```bash
$ yarn install # or npm install
```

To run the project:

```bash
$ yarn start # or npm start
```

## Description

This is a contact list app created with create-react-app.

## Technologies

Project uses:

- React(class components)
- styled-components
- API
- Tools: ESlint, Prettier
