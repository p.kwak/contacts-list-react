export const BREAKPOINTS = {
  mobile: '320px',
  tablet: '768px',
  laptop: '1024px'
};

export const FONT_SIZES = {
  sizeS: '1.3rem',
  sizeM: '1.8rem',
  sizeL: '2rem'
};

export const standardTheme = {
  ...BREAKPOINTS,
  ...FONT_SIZES,
  mainFontFamily: 'Montserrat, sans-serif',
  headerBG: '#2a9d8f',
  fontWhite: '#fff',
  fontDark: '#03071e',
  borderColor: '#C3C7C8'
};
