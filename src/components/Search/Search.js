import React from 'react';
import styled from 'styled-components';
import search from '../../assets/search.svg';

const SearchBar = styled.form`
  position: relative;
`;

const SearchInput = styled.input`
  position: relative;
  width: 100%;
  height: 40px;
  border: 1px solid transparent;
  padding-left: 20px;
  outline: none;
  transition: 0.3s;

  &:focus {
    color: #191919;
    box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.3),
      0 4px 6px -2px rgba(0, 0, 0, 0.06);
    outline: none;
  }
`;

const SearchIcon = styled.img`
  position: absolute;
  right: 20px;
  top: 10px;
  opacity: 0.5;
  width: 20px;
  height: 20px;
  transition: 0.3s;
  cursor: pointer;

  &:hover {
    transform: scale(1.1);
  }
`;

const Search = ({ onChange }) => (
  <>
    <SearchBar>
      <SearchInput type="text" placeholder="Search" onChange={onChange} />
      <SearchIcon src={search} alt="search icon" />
    </SearchBar>
  </>
);

export default Search;
