import styled from 'styled-components';

export const ContactsList = styled.ul`
  list-style: none;
  font-size: ${({ theme }) => theme.sizeS};
  color: ${({ theme }) => theme.fontDark};
  padding: 0;

  @media (min-width: ${({ theme }) => theme.tablet}) {
    font-size: ${({ theme }) => theme.sizeM};
  }

  @media (min-width: ${({ theme }) => theme.laptop}) {
    font-size: ${({ theme }) => theme.sizeL};
  }
`;

export const Contact = styled.li`
  display: flex;
  align-items: center;
  font-size: ${({ theme }) => theme.sizeS};
  border-bottom: 1px solid ${({ theme }) => theme.borderColor};

  @media (min-width: ${({ theme }) => theme.tablet}) {
    font-size: ${({ theme }) => theme.sizeM};
  }
`;

export const ContactAvatarContainer = styled.div`
  padding: 5px;
`;

export const ContactLabel = styled.label`
  cursor: pointer;
  width: 170px;

  @media (min-width: ${({ theme }) => theme.tablet}) {
    width: 300px;
  }
`;

export const ContactAvatar = styled.img`
  font-size: 1rem;
  width: 40px;
  height: 40px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

export const Checkbox = styled.input`
  margin: 0 0 0 20px;
  cursor: pointer;
`;
