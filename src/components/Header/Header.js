import React from 'react';
import styled from 'styled-components';
import Title from '../Title';

const StyledHeader = styled.header`
  width: 100%;
  background: ${({ theme }) => theme.headerBG};
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Header = () => (
  <StyledHeader>
    <Title>Contacts</Title>
  </StyledHeader>
);

export default Header;
