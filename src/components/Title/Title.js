import styled from 'styled-components';

const Title = styled.h1`
  padding: 1rem;
  font-size: ${({ theme }) => theme.sizeS};
  color: ${({ theme }) => theme.fontWhite};
  transition: all 0.3s;

  @media (min-width: ${({ theme }) => theme.tablet}) {
    font-size: ${({ theme }) => theme.sizeL};
  }
`;

export default Title;
