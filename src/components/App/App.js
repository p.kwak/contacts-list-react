/* eslint-disable no-console */
/* eslint-disable camelcase */
import React, { Component } from 'react';
import styledNormalize from 'styled-normalize';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import { standardTheme } from '../../constants/styles';
import Loader from '../Loader';
import Header from '../Header';
import Search from '../Search';
import {
  ContactsList,
  Contact,
  ContactAvatarContainer,
  ContactLabel,
  ContactAvatar,
  Checkbox
} from '../ContactsList';

const GlobalStyle = createGlobalStyle`
  ${styledNormalize}

  * {
    box-sizing: border-box;
    font-family: ${({ theme }) => theme.mainFontFamily};
  }

  html {
   height: 100%;
   width: 100%;
   font-size: 62.5%;
}

  body {
    min-height: 100%;
    font-size: 1.6rem;
  }
`;

const API =
  'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';

class App extends Component {
  state = {
    isLoading: true,
    data: [],
    searchTerm: ''
  };

  componentDidMount() {
    this.handleDataFetch();
  }

  handleDataFetch = () => {
    fetch(API)
      .then((response) => {
        if (response.ok) {
          return response;
        }
        throw Error(response.status);
      })
      .then((response) => response.json())
      .then((data) => {
        /**
         * @description
         * Returns a function which will sort an
         * array of objects by the given key.
         *
         * @param  {String}  key
         * @param  {Boolean} reverse
         * @return {Function}
         */
        const sortBy = (key, reverse) => {
          // Move smaller items towards the front
          // or back of the array depending on if
          // we want to sort the array in reverse
          // order or not.
          const moveSmaller = reverse ? 1 : -1;

          // Move larger items towards the front
          // or back of the array depending on if
          // we want to sort the array in reverse
          // order or not.
          const moveLarger = reverse ? -1 : 1;

          /**
           * @param  {*} a
           * @param  {*} b
           * @return {Number}
           */
          return (a, b) => {
            if (a[key] < b[key]) {
              return moveSmaller;
            }
            if (a[key] > b[key]) {
              return moveLarger;
            }
            return 0;
          };
        };

        data.sort(sortBy('last_name'));

        this.setState({
          data,
          isLoading: false
        });
      })
      .catch((error) => console.log(error));
  };

  render() {
    const { isLoading, searchTerm, data: contacts } = this.state;
    return (
      <ThemeProvider theme={standardTheme}>
        <GlobalStyle />
        {isLoading ? (
          <Loader />
        ) : (
          <>
            <Header />
            <Search
              onChange={(e) => this.setState({ searchTerm: e.target.value })}
            />
            <ContactsList>
              {contacts
                .filter((contact) => {
                  if (searchTerm === '') {
                    return contact;
                  }
                  if (
                    contact.first_name
                      .toLowerCase()
                      .includes(searchTerm.toLowerCase())
                  ) {
                    return contact;
                  }
                  if (
                    contact.last_name
                      .toLowerCase()
                      .includes(searchTerm.toLowerCase())
                  ) {
                    return contact;
                  }
                  return null;
                })
                .map((contact) => {
                  const { avatar, first_name, last_name, id } = contact;
                  return (
                    <Contact key={id}>
                      <ContactAvatarContainer>
                        <ContactAvatar src={avatar} alt="avatar" />
                      </ContactAvatarContainer>
                      <ContactLabel htmlFor={id}>
                        {first_name} {last_name}
                      </ContactLabel>
                      <Checkbox
                        type="checkbox"
                        name="userID"
                        id={id}
                        onClick={() => console.log('userID :', id)}
                      />
                    </Contact>
                  );
                })}
            </ContactsList>
          </>
        )}
      </ThemeProvider>
    );
  }
}

export default App;
